Problem Statement Backend / Fullstack Engineer

Anda diminta untuk membuat sebuah sistem order tiket acara:
1. User melakukan checkout tiket acara
2. User melakukan pembayaran dari tiket acara yang sudah di-checkout tersebut

Hal-hal yang harus dikumpulkan pada test ini adalah
1. Desain REST API yang akan dikembangkan
2. Desain Database untuk bisa handle flow tersebut
3. Implementasi REST API pada bahasa dan framework yang sering anda gunakan

Prosedur Pengumpulan
1. Buat repository public yang dapat diakses oleh tim
2. Upload source code anda di repository tersebut
3. Berikan contoh hasil run API yang anda buat beserta format request dan response
4. Desain REST API dan Database, dapat dilampirkan di dalam email pengumpulan
5. Kirim ke https://forms.gle/siscE1nya7J2rfKM6

Gunakan bahasa pemrograman GoLang.
Apabila ada pertanyaan lanjutan, silahkan disampaikan ke Sela (0895381033293)