package main

import (
	"fmt"
	"svc-order-ticket-go/config"
	"svc-order-ticket-go/routes"

	"github.com/gin-gonic/gin"
)

func main() {
	// Capture database connection
	sqlDB := config.SqlConnection()
	gormDB := config.GormConnection(sqlDB)
	fmt.Println("Connected!")

	app := config.App{
		SqlDB:  sqlDB,
		GormDB: gormDB,
	}

	// Run server
	router := gin.Default()
	routes.InitRoute(router, &app)
	router.Run("localhost:8080")
}
