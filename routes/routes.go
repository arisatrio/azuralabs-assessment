package routes

import (
	"svc-order-ticket-go/config"
	examplemodule "svc-order-ticket-go/modules/example-module"
	"svc-order-ticket-go/modules/order"
	orderhistory "svc-order-ticket-go/modules/order-history"
	paymentmethod "svc-order-ticket-go/modules/payment-method"
	settingmodule "svc-order-ticket-go/modules/setting-module"
	"svc-order-ticket-go/modules/ticket"

	"github.com/gin-gonic/gin"
)

func InitRoute(gin *gin.Engine, app *config.App) {
	publicGroupPath := gin.Group("/api")
	_ = gin.Group("/private/api")
	publicGroupPath.GET("/test", examplemodule.HandleTestApp(app))

	publicGroupPath.POST("/checkout", ticket.HandleCheckoutTicket(app))
	publicGroupPath.PUT("/payment", ticket.HandlePaymentTicket(app))

	publicGroupPath.GET("/ticket", ticket.HandleGetAvailableTicket(app))
	publicGroupPath.GET("/search", ticket.HandleGetTickets(app))
	publicGroupPath.GET("/orders", order.HandleGetOrders(app))
	publicGroupPath.GET("/settings", settingmodule.HandleGetSettings(app))
	publicGroupPath.PUT("/setting", settingmodule.HandleUpdateSetting(app))
	publicGroupPath.GET("/payment-methods", paymentmethod.HandleGetPaymentMethod(app))
	publicGroupPath.PUT("/payment-method/:methodCode", paymentmethod.HandleUpdatePaymentMethod(app))
	publicGroupPath.GET("/order-history/:ticketNumber", orderhistory.HandleGetHistory(app))

}
