package statusorder

import (
	"fmt"
	"svc-order-ticket-go/config"
)

func GetStatusOrderByCode(app *config.App, statusCode string) (*StatusOrder, error) {
	var statusOrder StatusOrder
	result := app.GormDB.Where("status_code = ?", statusCode).Find(&statusOrder)
	if result.Error != nil {
		fmt.Printf("[ERROR] Get satus order - %v at modules/status-order/status_order_repository.go - GetStatusOrderByCode()\n", result.Error)
		return nil, result.Error
	}
	if result.RowsAffected == 0 {
		fmt.Printf("[ERROR] Get satus order - 'status order %v not found' at modules/status-order/status_order_repository.go - GetStatusOrderByCode()\n", statusCode)
		return nil, fmt.Errorf("")
	}
	return &statusOrder, nil
}
