package statusorder

const (
	CodeStatusBooked  = "BK"
	CodeStatusExpired = "EX"
	CodeStatusPaid    = "PD"
)

type StatusOrder struct {
	ID         string `json:"id"`
	StatusCode string `json:"status_code"`
	StatusName string `json:"status_name"`
	// FlagActive string `json:"flag_active"`
	// FlagDelete string `json:"flag_delete"`
	// CreatedAt  string `json:"created_at"`
	// CreatedBy  string `json:"created_by"`
	// UpdatedAt  string `json:"updated_at"`
	// UpdatedBy  string `json:"updated_by"`
}

func DefaultStatusOrder() string {
	return CodeStatusBooked
}
