package ticket

import (
	"svc-order-ticket-go/config"
	"svc-order-ticket-go/modules/order"
	settingmodule "svc-order-ticket-go/modules/setting-module"
	statusorder "svc-order-ticket-go/modules/status-order"
	"time"
)

func GenerateTicketInfo(app *config.App, orderDetail *order.OrderDetail) (*TicketBill, error) {
	paymentDuration, err := settingmodule.GetSettingByName(app, settingmodule.AttributeDurationLimit)
	if err != nil {
		return nil, err
	}
	layout := "2006-01-02 15:04:05"
	createAt, _ := time.Parse(layout, orderDetail.CreatedAt)
	ticket := TicketBill{
		TicketNumber:  orderDetail.TicketNumber,
		UserName:      orderDetail.User,
		Price:         orderDetail.Price,
		Status:        orderDetail.StatusName,
		PaymentMethod: orderDetail.MethodName,
		AccountNumber: orderDetail.AccountNumber,
		ExpiredAt:     createAt.Add(time.Duration(paymentDuration.Value) * time.Second).Format(layout),
	}
	return &ticket, nil
}

func chekingExpiredTicket(app *config.App, ticketNumber string) {
	paymentDuration, _ := settingmodule.GetSettingByName(app, settingmodule.AttributeDurationLimit)
	time.Sleep(time.Duration(paymentDuration.Value) * time.Second)
	statusExpired, _ := statusorder.GetStatusOrderByCode(app, statusorder.CodeStatusExpired)
	app.GormDB.Model(&order.Order{}).Where("ticket_number", ticketNumber).Update("id_status_order", statusExpired.ID)
}
