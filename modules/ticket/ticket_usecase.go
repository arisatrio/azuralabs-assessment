package ticket

import (
	"database/sql"
	"fmt"
	"net/http"
	"strconv"
	"svc-order-ticket-go/config"
	"svc-order-ticket-go/modules/order"
	orderhistory "svc-order-ticket-go/modules/order-history"
	settingmodule "svc-order-ticket-go/modules/setting-module"
	statusorder "svc-order-ticket-go/modules/status-order"

	"github.com/gin-gonic/gin"
)

const defaultTotalOrder = 1

func HandleGetAvailableTicket(app *config.App) gin.HandlerFunc {
	handler := func(c *gin.Context) {
		availableTickets, err := GetAvailabelTicket(app)
		if err != nil {
			c.JSON(http.StatusInternalServerError, map[string]string{"message": "internal server error"})
			return
		}
		c.JSON(http.StatusOK, config.Response{
			Message: "success",
			Data:    map[string]interface{}{"available_tickets": availableTickets},
		})
	}

	return gin.HandlerFunc(handler)
}

func HandleGetTickets(app *config.App) gin.HandlerFunc {
	handler := func(c *gin.Context) {
		ticketNumber := c.Query("keyword")
		if ticketNumber == "" {
			c.JSON(http.StatusBadRequest, map[string]string{"message": "harap masukkan nomor tiket"})
			return
		}
		myTicket, err := order.GetOrderDetailByTicketNumber(app, ticketNumber)
		if err != nil {
			if err == sql.ErrNoRows {
				c.JSON(http.StatusOK, map[string]interface{}{"message": fmt.Sprintf("ticket %s not found", ticketNumber)})
				return
			}
			c.JSON(http.StatusInternalServerError, map[string]string{"message": "internal server error"})
			return
		}
		ticketInfo, err := GenerateTicketInfo(app, myTicket)
		if err != nil {
			c.JSON(http.StatusInternalServerError, map[string]string{"message": "internal server error"})
			return
		}
		c.JSON(http.StatusOK, config.Response{
			Message: "success",
			Data:    ticketInfo,
		})
	}

	return gin.HandlerFunc(handler)
}

func HandleCheckoutTicket(app *config.App) gin.HandlerFunc {
	handler := func(c *gin.Context) {
		request := order.RequestOrder{}
		err := c.Bind(&request)
		if err != nil {
			fmt.Println("[ERROR] error binding request at modules/ticket/ticket_usecase.go - HandleCheckoutTicket()")
			c.JSON(http.StatusInternalServerError, config.Response{Message: "internal server error"})
			return
		}
		customError := validateOrderTicket(app, &request)
		if customError != nil {
			c.JSON(customError.Code, config.Response{Message: customError.Message})
			return
		}
		orders, err := order.PrepareCreateOrder(app, &request)
		if err != nil {
			c.JSON(http.StatusInternalServerError, config.Response{Message: "internal server error"})
			return
		}
		if len(orders) > 0 {
			for _, v := range orders {
				go chekingExpiredTicket(app, v.TicketNumber)
			}
		}
		err = orderhistory.PrepareCreateOrderHistory(app, orders)
		if err != nil {
			c.JSON(http.StatusInternalServerError, config.Response{Message: "internal server error"})
			return
		}
		response, err := ResponseCheckoutTicket(app, orders)
		if err != nil {
			c.JSON(http.StatusInternalServerError, config.Response{Message: "internal server error"})
			return
		}

		//
		//	TODO make the command process above be in transactions
		//	All the checkout and payment process should be move in a transaction,
		//	besause there are multiple transaction is depend on each other. So, if one
		//	process is failed, previously completed process must be rollback.
		//

		c.JSON(http.StatusOK, config.Response{
			Message: "success",
			Data:    response,
		})
	}

	return gin.HandlerFunc(handler)
}

func HandlePaymentTicket(app *config.App) gin.HandlerFunc {
	handler := func(c *gin.Context) {
		var message string
		type ticket struct {
			TicketNumber string `json:"ticket_number" form:"ticket_number"`
		}
		tickets := []ticket{}
		c.Bind(&tickets)
		statusOrderPaid, err := statusorder.GetStatusOrderByCode(app, statusorder.CodeStatusPaid)
		if err != nil {
			c.JSON(http.StatusInternalServerError, config.Response{Message: "internal server error"})
			return
		}
		var postOrders []order.PostOrder
		for _, v := range tickets {
			detailTicket, err := order.GetOrderDetailByTicketNumber(app, v.TicketNumber)
			if err != nil {
				c.JSON(http.StatusInternalServerError, config.Response{Message: "internal server error"})
				return
			}
			if detailTicket.IsBooked() {
				var dataOrder order.Order
				app.GormDB.Model(&order.Order{}).Where("ticket_number", detailTicket.TicketNumber).Update("id_status_order", statusOrderPaid.ID)
				app.GormDB.Table("orders").Where("ticket_number", detailTicket.TicketNumber).First(&dataOrder)
				postOrder := order.PostOrder{
					ID:            dataOrder.ID,
					TicketNumber:  dataOrder.TicketNumber,
					IdStatusOrder: dataOrder.IdStatusOrder,
				}
				postOrders = append(postOrders, postOrder)
			}
			if detailTicket.IsExpired() {
				message = fmt.Sprintf("ticked %s expired", detailTicket.TicketNumber)
				continue
			}
			if detailTicket.IsSold() {
				message = fmt.Sprintf("no bill for %s", detailTicket.TicketNumber)
				continue
			}
		}
		if postOrders == nil {
			c.JSON(http.StatusOK, map[string]string{"message": message})
			return
		}
		err = orderhistory.PrepareCreateOrderHistory(app, postOrders)
		if err != nil {
			c.JSON(http.StatusInternalServerError, config.Response{Message: "internal server error"})
			return
		}
		response, err := ResponseCheckoutTicket(app, postOrders)
		if err != nil {
			c.JSON(http.StatusInternalServerError, config.Response{Message: "internal server error"})
			return
		}
		c.JSON(http.StatusOK, config.Response{
			Message: "success",
			Data:    response,
		})

	}

	return gin.HandlerFunc(handler)
}

func GetAvailabelTicket(app *config.App) (int, error) {
	totalKuota, err := settingmodule.GetSettingByName(app, attributeNameKuota)
	if err != nil {
		return 0, err
	}
	soldTotal, err := order.CountTicketOrdersByStatus(app, codeStatusSold)
	if err != nil {
		return 0, err
	}
	bookedTotal, err := order.CountTicketOrdersByStatus(app, codeStatusBooked)
	if err != nil {
		return 0, err
	}
	availableTickets := totalKuota.Value - soldTotal - bookedTotal

	return availableTickets, nil
}

func validateOrderTicket(app *config.App, request *order.RequestOrder) *config.ResponseError {
	quantity, _ := strconv.Atoi(request.Quantity)
	if quantity > defaultTotalOrder {
		multipleOrder, err := settingmodule.GetSettingByName(app, settingmodule.AttributeFlagMultipleOrder)
		if err != nil {
			return &config.ResponseError{
				Code:    config.HttpCodeError,
				Message: "internal server error",
			}
		}
		if multipleOrder.DisableMultipleOrder() {
			fmt.Println("[INFO] Order rejected, multiple ticket order is disable")
			return &config.ResponseError{
				Code:    config.HttpCodeStatusOK,
				Message: "maksimal pembelian tiket adalah 1",
			}
		}
	}
	availableTickets, err := GetAvailabelTicket(app)
	if err != nil {
		return &config.ResponseError{
			Code:    config.HttpCodeError,
			Message: "internal server error",
		}
	}
	if IsTicketSoldOut(availableTickets) {
		fmt.Println("[INFO] Order rejected, tickets sold out")
		return &config.ResponseError{
			Code:    config.HttpCodeStatusOK,
			Message: "tiket terjual habis",
		}
	}
	if quantity > availableTickets {
		fmt.Println("[INFO] order rejected, tiket yang tersedia tidak mencukupi")
		return &config.ResponseError{
			Code:    config.HttpCodeStatusOK,
			Message: "jumlah tiket yang tersedia tidak mencukupi",
		}
	}

	return nil
}

func ResponseCheckoutTicket(app *config.App, orders []order.PostOrder) ([]TicketBill, error) {
	var ticketBills []TicketBill
	for _, v := range orders {
		orderDetail, err := order.GetOrderDetailByTicketNumber(app, v.TicketNumber)
		if err != nil {
			return nil, err
		}
		ticketInfo, err := GenerateTicketInfo(app, orderDetail)
		if err != nil {
			return nil, err
		}
		ticketBills = append(ticketBills, *ticketInfo)
	}
	return ticketBills, nil
}
