package ticket

const (
	attributeNameKuota = "kuota"
	attributeNamePrice = "price"
	codeStatusBooked   = "BK"
	codeStatusSold     = "PD"
)

type TicketBill struct {
	TicketNumber  string `json:"ticket_number"`
	UserName      string `json:"name"`
	Price         string `json:"price"`
	Status        string `json:"status"`
	PaymentMethod string `json:"payment_method"`
	AccountNumber string `json:"account_number"`
	ExpiredAt     string `json:"expired_at"`
}

func IsTicketSoldOut(tickets int) bool {
	return tickets < 1
}
