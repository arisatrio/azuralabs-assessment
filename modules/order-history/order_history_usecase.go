package orderhistory

import (
	"net/http"
	"svc-order-ticket-go/config"

	"github.com/gin-gonic/gin"
)

func HandleGetHistory(app *config.App) gin.HandlerFunc {
	handler := func(c *gin.Context) {
		ticketNumber := c.Param("ticketNumber")
		orderHistory, err := GetOrderHistory(app, ticketNumber)
		if err != nil {
			c.JSON(http.StatusInternalServerError, config.Response{Message: "internal server error"})
			return
		}
		c.JSON(http.StatusOK, config.Response{
			Message: "success",
			Data:    orderHistory,
		})
	}

	return gin.HandlerFunc(handler)
}
