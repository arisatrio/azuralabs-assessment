package orderhistory

import "fmt"

func GetOrderHistoryQuery(filter string) string {
	return fmt.Sprintf(
		`SELECT
		o.user,
		o.ticket_number,
		o.price,
		oh.flag_active,
		oh.flag_delete,
		oh.created_at,
		oh.created_by,
		oh.updated_at,
		oh.updated_by,
		so.status_code,
		so.status_name,
		pm.method_code,
		pm.method_name,
		pm.account_number
		FROM order_histories oh
		JOIN orders o ON oh.id_order = o.id
		JOIN status_orders so ON oh.id_status_order = so.id
		Join payment_methods pm ON o.id_payment_method = pm.id
		%s`, filter,
	)
}
