package orderhistory

type OrderHistory struct {
	OrderID       int64
	StatusOrderID string
	CreatedAt     string
	UpdatedAt     string
}

type PostOrderHistory struct {
	IdOrder       int64  `json:"id_order"`
	IdStatusOrder string `json:"id_status_order"`
}

// type FullOrderHistory struct {
// 	ID            string `json:"id"`
// 	IdOrder       string `json:"id_order"`
// 	IdStatusOrder string `json:"id_status_order"`
// 	FlagActive    string `json:"flag_active"`
// 	FlagDelete    string `json:"flag_delete"`
// 	CreatedAt     string `json:"created_at"`
// 	CreatedBy     string `json:"created_by"`
// 	UpdatedAt     string `json:"updated_at"`
// 	UpdatedBy     string `json:"updated_by"`
// }

// type OrderHistoryDetail struct {
// 	order.Order
// 	FullOrderHistory
// 	statusorder.StatusOrder
// 	paymentmethod.PaymentMethod
// }

type OrderHistoryDetail struct {
	User          string `json:"user"`
	TicketNumber  string `json:"ticket_number"`
	Price         string `json:"price"`
	FlagActive    string `json:"flag_active"`
	FlagDelete    string `json:"flag_delete"`
	CreatedAt     string `json:"created_at"`
	CreatedBy     string `json:"created_by"`
	UpdatedAt     string `json:"updated_at"`
	UpdatedBy     string `json:"updated_by"`
	StatusCode    string `json:"status_code"`
	StatusName    string `json:"status_name"`
	MethodCode    string `json:"method_code"`
	MethodName    string `json:"method_name"`
	AccountNumber string `json:"account_number"`
}
