package orderhistory

import (
	"fmt"
	"svc-order-ticket-go/config"
	"svc-order-ticket-go/modules/order"
)

func PrepareCreateOrderHistory(app *config.App, postOrder []order.PostOrder) error {
	var postOrderHistories []PostOrderHistory
	for _, v := range postOrder {
		postOrderHistory := PostOrderHistory{
			IdOrder:       v.ID,
			IdStatusOrder: v.IdStatusOrder,
		}
		postOrderHistories = append(postOrderHistories, postOrderHistory)
	}
	err := CreateOrderHistory(app, postOrderHistories)
	if err != nil {
		return err
	}
	return nil
}

func CreateOrderHistory(app *config.App, postOrderHistory []PostOrderHistory) error {
	result := app.GormDB.Table("order_histories").Create(&postOrderHistory)
	if result.Error != nil {
		fmt.Printf("[ERROR] error create history order - %v at modules/order-history/order_history_repository.go - CreateOrderHistory()\n", result.Error)
		return result.Error
	}
	return nil
}

func GetOrderHistory(app *config.App, ticketNumber string) ([]*OrderHistoryDetail, error) {
	var orderHistoryDetails []*OrderHistoryDetail
	filter := fmt.Sprintf("WHERE o.ticket_number = %q", ticketNumber)
	sqlQuery := GetOrderHistoryQuery(filter)
	rows, err := app.SqlDB.Query(sqlQuery)
	if err != nil {
		fmt.Printf("[ERROR] get data order history - %s while call function 'Query' at modules/order-history/order_history_repository.go - GetOrderHistoryByTicketNumber", err)
	}
	for rows.Next() {
		var orderHistoryDetail OrderHistoryDetail
		err := rows.Scan(
			&orderHistoryDetail.User,
			&orderHistoryDetail.TicketNumber,
			&orderHistoryDetail.Price,
			&orderHistoryDetail.FlagActive,
			&orderHistoryDetail.FlagDelete,
			&orderHistoryDetail.CreatedAt,
			&orderHistoryDetail.CreatedBy,
			&orderHistoryDetail.UpdatedAt,
			&orderHistoryDetail.UpdatedBy,
			&orderHistoryDetail.StatusCode,
			&orderHistoryDetail.StatusName,
			&orderHistoryDetail.MethodCode,
			&orderHistoryDetail.MethodName,
			&orderHistoryDetail.AccountNumber,
		)
		if err != nil {
			fmt.Printf("[ERROR] error get data order history - %s while call function 'Scan' at modules/order/order_history_repository.go - GetOrderHistory()\n", err)
			return nil, err
		}
		orderHistoryDetails = append(orderHistoryDetails, &orderHistoryDetail)
	}
	if err := rows.Err(); err != nil {
		fmt.Printf("[ERROR] error get data order history - %s at modules/order/order_history_repository.go - GetOrderHistory()\n", err)
		return nil, err
	}
	return orderHistoryDetails, nil
}
