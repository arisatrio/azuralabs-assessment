package order

import "fmt"

func GetOrderDetailQuery(filter string) string {
	return fmt.Sprintf(
		`SELECT
		o.id,
		o.user,
		o.ticket_number,
		o.price,
		o.id_payment_method,
		o.id_status_order,
		o.flag_active,
		o.flag_delete,
		o.created_at,
		o.created_by,
		o.updated_at,
		o.updated_by,
		so.id,
		status_code,
		so.status_name,
		pm.id,
		pm.method_code,
		pm.method_name,
		pm.account_number
		FROM orders o
		JOIN status_orders so ON o.id_status_order = so.id
		JOIN payment_methods pm ON o.id_payment_method = pm.id
		%s`, filter,
	)
}
