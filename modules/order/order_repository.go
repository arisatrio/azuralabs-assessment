package order

import (
	"database/sql"
	"fmt"
	"math/rand"
	"strconv"
	"svc-order-ticket-go/config"
	paymentmethod "svc-order-ticket-go/modules/payment-method"
	settingmodule "svc-order-ticket-go/modules/setting-module"
	statusorder "svc-order-ticket-go/modules/status-order"
)

func GetOrderByTicketNumber(app *config.App, ticketNumber string) (*Order, error) {
	dataOrder := Order{}
	result := app.GormDB.Where("ticket_number = ?", ticketNumber).First(&dataOrder)
	if result.Error != nil {
		fmt.Printf("[ERROR] error get order - %v at modules/order/order_repository.go - GetOrderByTicketNumber()\n", result.Error)
		return nil, result.Error
	}
	if result.RowsAffected == 0 {
		fmt.Printf("[ERROR] no row affected -  ticket numbers %v not found at modules/order/order_repository.go - GetOrderByTicketNumber()\n", ticketNumber)
		return nil, fmt.Errorf("")
	}
	return &dataOrder, nil
}

func GetOrderDetail(app *config.App) ([]OrderDetail, error) {
	var orderDetails []OrderDetail
	sqlQuery := GetOrderDetailQuery("")
	rows, err := app.SqlDB.Query(sqlQuery)
	if err != nil {
		fmt.Printf("[ERROR] error get data order - %s while call function 'Query' at modules/order/order_repository.go/GetOrderDetail()\n", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var orderDetail OrderDetail
		err := rows.Scan(
			&orderDetail.Order.ID,
			&orderDetail.Order.User,
			&orderDetail.Order.TicketNumber,
			&orderDetail.Order.Price,
			&orderDetail.Order.IdPaymentMethod,
			&orderDetail.Order.IdStatusOrder,
			&orderDetail.Order.FlagActive,
			&orderDetail.Order.FlagDelete,
			&orderDetail.Order.CreatedAt,
			&orderDetail.Order.CreatedBy,
			&orderDetail.Order.UpdatedAt,
			&orderDetail.Order.UpdatedBy,
			&orderDetail.StatusOrder.ID,
			&orderDetail.StatusOrder.StatusCode,
			&orderDetail.StatusOrder.StatusName,
			&orderDetail.PaymentMethod.ID,
			&orderDetail.PaymentMethod.MethodCode,
			&orderDetail.PaymentMethod.MethodName,
			&orderDetail.PaymentMethod.AccountNumber,
		)
		if err != nil {
			fmt.Printf("[ERROR] error get data order - %s while call function 'Scan' at modules/order/order_repository.go/GetOrderDetail()\n", err)
			return nil, err
		}
		orderDetails = append(orderDetails, orderDetail)
	}
	if err := rows.Err(); err != nil {
		fmt.Printf("[ERROR] error get data order - %s at modules/order/order_repository.go/GetOrderDetail()\n", err)
		return nil, err
	}
	return orderDetails, nil
}

func GetOrderDetailByTicketNumber(app *config.App, ticketNumber string) (*OrderDetail, error) {
	var orderDetail OrderDetail
	filter := fmt.Sprintf("WHERE o.ticket_number = %q", ticketNumber)
	sqlQuery := GetOrderDetailQuery(filter)
	row := app.SqlDB.QueryRow(sqlQuery)
	err := row.Scan(
		&orderDetail.Order.ID,
		&orderDetail.Order.User,
		&orderDetail.Order.TicketNumber,
		&orderDetail.Order.Price,
		&orderDetail.Order.IdPaymentMethod,
		&orderDetail.Order.IdStatusOrder,
		&orderDetail.Order.FlagActive,
		&orderDetail.Order.FlagDelete,
		&orderDetail.Order.CreatedAt,
		&orderDetail.Order.CreatedBy,
		&orderDetail.Order.UpdatedAt,
		&orderDetail.Order.UpdatedBy,
		&orderDetail.StatusOrder.ID,
		&orderDetail.StatusOrder.StatusCode,
		&orderDetail.StatusOrder.StatusName,
		&orderDetail.PaymentMethod.ID,
		&orderDetail.PaymentMethod.MethodCode,
		&orderDetail.PaymentMethod.MethodName,
		&orderDetail.PaymentMethod.AccountNumber,
	)
	if err != nil {
		if err == sql.ErrNoRows {
			fmt.Println("[INFO] no rows affected for ticket number: ", ticketNumber)
			return nil, err
		}
		fmt.Printf("[ERROR] error get data order - %s while call function 'Scan' at modules/order/order_repository.go/GetOrderDetail()\n", err)
		return nil, err
	}
	return &orderDetail, nil
}

func PrepareCreateOrder(app *config.App, request *RequestOrder) ([]PostOrder, error) {
	price, err := settingmodule.GetSettingByName(app, settingmodule.AttributePrice)
	if err != nil {
		return nil, err
	}
	typePayment := paymentmethod.DefaultPaymentMethod()
	if request.PaymentMethod != "" {
		typePayment = request.PaymentMethod
	}
	paymentMethod, err := paymentmethod.GetPaymentMethodByCode(app, typePayment)
	if err != nil {
		return nil, err
	}
	statusOrder, err := statusorder.GetStatusOrderByCode(app, statusorder.DefaultStatusOrder())
	if err != nil {
		return nil, err
	}
	var postOrders []PostOrder
	quantity := request.CheckQuantity()
	for i := 0; i < quantity; i++ {
		dataOrder := Order{}
		dataOrder.User = request.Name
		if dataOrder.User == "" {
			dataOrder.User = "unkonwn"
		}
		// dataOrder.TicketNumber = strings.ToUpper(RandomString(8))
		dataOrder.TicketNumber = NewRandonString(4)
		fmt.Println(dataOrder.TicketNumber)
		dataOrder.Price = strconv.Itoa(price.Value)
		dataOrder.IdPaymentMethod = paymentMethod.ID
		dataOrder.IdStatusOrder = statusOrder.ID
		postOrder := PostOrder{
			User:            dataOrder.User,
			TicketNumber:    dataOrder.TicketNumber,
			Price:           dataOrder.Price,
			IdPaymentMethod: dataOrder.IdPaymentMethod,
			IdStatusOrder:   dataOrder.IdStatusOrder,
		}
		postOrders = append(postOrders, postOrder)
	}
	err = CreateOrder(app, postOrders)
	if err != nil {
		return nil, err
	}
	return postOrders, nil
}

func CreateOrder(app *config.App, postOrder []PostOrder) error {
	result := app.GormDB.Table("orders").Create(postOrder)
	if result.Error != nil {
		fmt.Printf("[ERROR] error create order - %v at modules/order/order_repository.go - CreateOrder()\n", result.Error)
		return result.Error
	}
	return nil
}

func RandomString(length int) string {
	var letters = []rune("ABCDEFGHIJKLMNPQRSTUVWXYZ123456789")
	b := make([]rune, length)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func NewRandonString(length int) string {
	b := make([]byte, length)
	if _, err := rand.Read(b); err != nil {
		panic(err)
	}
	s := fmt.Sprintf("%X", b)
	return s
}
