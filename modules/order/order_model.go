package order

import (
	"strconv"
	paymentmethod "svc-order-ticket-go/modules/payment-method"
	statusorder "svc-order-ticket-go/modules/status-order"
)

const (
	StatusCodeBooked  = "BK"
	StatusCodeSold    = "PD"
	StatusCodeExpired = "EX"
	StatusCodeFailed  = "FL"
)

type Order struct {
	ID              int64  `json:"id,omitempty" form:"id"`
	User            string `json:"user,omitempty" form:"user"`
	TicketNumber    string `json:"ticket_number,omitempty" form:"ticket_number"`
	Price           string `json:"price,omitempty" form:"price"`
	IdPaymentMethod string `json:"id_payment_method,omitempty" form:"id_payment_method"`
	IdStatusOrder   string `json:"id_status_order,omitempty" form:"id_status_order"`
	FlagActive      string `json:"flag_active,omitempty" form:"flag_active"`
	FlagDelete      string `json:"flag_delete,omitempty" form:"flag_delete"`
	CreatedAt       string `json:"created_at,omitempty" form:"created_at"`
	CreatedBy       string `json:"created_by,omitempty" form:"created_by"`
	UpdatedAt       string `json:"updated_at,omitempty" form:"updated_at"`
	UpdatedBy       string `json:"updated_by,omitempty" form:"updated_by"`
	// RequestOrder
}

type RequestOrder struct {
	Quantity      string `json:"quantity,omitempty" form:"quantity"`
	Name          string `json:"name,omitempty" form:"name"`
	PaymentMethod string `json:"payment_method,omitempty" form:"payment_method"`
}

type PostOrder struct {
	ID              int64  `json:"-"`
	User            string `json:"user"`
	TicketNumber    string `json:"ticket_number"`
	Price           string `json:"price"`
	IdPaymentMethod string `json:"id_payment_method"`
	IdStatusOrder   string `json:"id_status_order"`
}

type OrderDetail struct {
	Order
	statusorder.StatusOrder
	paymentmethod.PaymentMethod
}

func (orderDetail *OrderDetail) IsBooked() bool {
	return orderDetail.StatusOrder.StatusCode == StatusCodeBooked
}

func (orderDetail *OrderDetail) IsSold() bool {
	return orderDetail.StatusOrder.StatusCode == StatusCodeSold
}

func (orderDetail *OrderDetail) IsExpired() bool {
	return orderDetail.StatusOrder.StatusCode == statusorder.CodeStatusExpired
}

func (request *RequestOrder) CheckQuantity() int {
	if request.Quantity == "" {
		return 1
	}
	qty, _ := strconv.Atoi(request.Quantity)
	return qty
}

func (request *RequestOrder) IsDefaultOrderTicket() bool {
	return request.Quantity == "1"
}

func (request *RequestOrder) IsMultipleOrderTicket() bool {
	return request.Quantity > "1"
}
