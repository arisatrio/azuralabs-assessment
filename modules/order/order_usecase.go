package order

import (
	"net/http"
	"svc-order-ticket-go/config"

	"github.com/gin-gonic/gin"
)

func HandleGetOrders(app *config.App) gin.HandlerFunc {
	handler := func(c *gin.Context) {
		orders, err := GetOrderDetail(app)
		if err != nil {
			c.JSON(http.StatusInternalServerError, config.Response{Message: "internal server error"})
			return
		}
		c.JSON(200, config.Response{
			Message: "seccess",
			Data:    orders,
		})
	}
	return gin.HandlerFunc(handler)
}

func CountTicketOrdersByStatus(app *config.App, statusCode string) (int, error) {
	count := 0
	orderDetails, err := GetOrderDetail(app)
	if err != nil {
		return 0, err
	}

	if statusCode == StatusCodeBooked {
		for _, v := range orderDetails {
			if v.IsBooked() {
				count++
			}
		}
		return count, nil
	}

	if statusCode == StatusCodeSold {
		for _, v := range orderDetails {
			if v.IsSold() {
				count++
			}
		}
		return count, nil
	}

	return count, nil
}
