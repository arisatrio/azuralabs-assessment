package paymentmethod

import (
	"fmt"
	"net/http"
	"svc-order-ticket-go/config"

	"github.com/gin-gonic/gin"
)

func HandleGetPaymentMethod(app *config.App) gin.HandlerFunc {
	handler := func(c *gin.Context) {
		paymentMethods, err := GetAllPaymentMethod(app)
		if err != nil {
			c.JSON(http.StatusInternalServerError, config.Response{Message: "internal_server_error"})
			return
		}
		c.JSON(http.StatusOK, config.Response{
			Message: "success",
			Data:    paymentMethods,
		})
	}

	return gin.HandlerFunc(handler)
}

func HandleUpdatePaymentMethod(app *config.App) gin.HandlerFunc {
	handler := func(c *gin.Context) {
		methodCode := c.Param("methodCode")
		paymentMethods, err := GetPaymentMethodByCode(app, methodCode)
		if err != nil {
			c.JSON(http.StatusInternalServerError, config.Response{Message: "internal_server_error"})
			return
		}
		c.Bind(&paymentMethods)
		fmt.Println(paymentMethods)
		err = UpdatePaymentMethod(app, paymentMethods)
		if err != nil {
			c.JSON(http.StatusInternalServerError, config.Response{Message: "internal_server_error"})
			return
		}
		c.JSON(http.StatusOK, config.Response{Message: "berhasil ubah data"})
	}

	return gin.HandlerFunc(handler)
}
