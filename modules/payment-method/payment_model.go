package paymentmethod

const (
	codeBankTransfer = "BTF"
)

type PaymentMethod struct {
	ID            string `json:"id" form:"id"`
	MethodCode    string `json:"method_code" form:"method_code"`
	MethodName    string `json:"method_name" form:"method_name"`
	AccountNumber string `json:"account_number" form:"account_number"`
	// FlagActive string `json:"flag_active"`
	// FlagDelete string `json:"flag_delete"`
	// CreatedAt  string `json:"created_at"`
	// CreatedBy  string `json:"created_by"`
	// UpdatedAt  string `json:"updated_at"`
	// UpdatedBy  string `json:"updated_by"`
}

func DefaultPaymentMethod() string {
	return codeBankTransfer
}
