package paymentmethod

import (
	"fmt"
	"svc-order-ticket-go/config"
)

func GetAllPaymentMethod(app *config.App) ([]*PaymentMethod, error) {
	var paymentMethods []*PaymentMethod
	result := app.GormDB.Where("flag_active = 1 AND flag_delete = 0").Find(&paymentMethods)
	if result.Error != nil {
		fmt.Printf("[ERROR] Get payment method - %v at modules/payment-method/payment_repository.go - GetAllPaymentMethodByCode()\n", result.Error)
		return nil, result.Error
	}
	return paymentMethods, nil
}

func GetPaymentMethodByCode(app *config.App, methodCode string) (*PaymentMethod, error) {
	var paymentMethod PaymentMethod
	result := app.GormDB.Where("method_code = ?", methodCode).Find(&paymentMethod)
	if result.Error != nil {
		fmt.Printf("[ERROR] Get payment method - %v at modules/payment-method/payment_repository.go - GetPaymentMethodByCode()\n", result.Error)
		return nil, result.Error
	}
	if result.RowsAffected == 0 {
		fmt.Printf("[ERROR] Get payment method - payment method '%v' not found at modules/payment-method/payment_method_repository.go - GetPaymentMethodByCode()\n", methodCode)
		return nil, fmt.Errorf("")
	}
	return &paymentMethod, nil
}

func UpdatePaymentMethod(app *config.App, paymentMethod *PaymentMethod) error {
	result := app.GormDB.Save(&paymentMethod)
	if result.Error != nil {
		fmt.Printf("[ERROR] update payment method - '%v' at modules/payment-method/payment_method_repository.go - GetPaymentMethodByCode()\n", result.Error)
		return result.Error
	}
	return nil
}
