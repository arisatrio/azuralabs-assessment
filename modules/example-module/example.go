package examplemodule

import (
	"fmt"
	"net/http"
	"svc-order-ticket-go/config"

	"github.com/gin-gonic/gin"
)

func HandleTestApp(app *config.App) gin.HandlerFunc {
	handler := func(c *gin.Context) {
		fmt.Println("Ready to serve")
		c.JSON(http.StatusOK, "ready to serve")
	}

	return gin.HandlerFunc(handler)
}
