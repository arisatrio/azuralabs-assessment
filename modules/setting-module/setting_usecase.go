package settingmodule

import (
	"net/http"
	"svc-order-ticket-go/config"

	"github.com/gin-gonic/gin"
)

func HandleGetSettings(app *config.App) gin.HandlerFunc {
	handler := func(c *gin.Context) {
		settings, err := GetAllSettings(app)
		if err != nil {
			c.JSON(http.StatusInternalServerError, config.Response{Message: "internal server error"})
			return
		}
		c.JSON(http.StatusOK, config.Response{
			Message: "success",
			Data:    settings,
		})
	}

	return gin.HandlerFunc(handler)
}

func HandleUpdateSetting(app *config.App) gin.HandlerFunc {
	handler := func(c *gin.Context) {
		attribute := c.Request.FormValue("attribute")
		setting, err := GetSettingByName(app, attribute)
		if err != nil {
			c.JSON(http.StatusInternalServerError, config.Response{Message: "internal server error"})
			return
		}
		c.Bind(&setting)
		err = UpdateSettings(app, setting)
		if err != nil {
			c.JSON(http.StatusInternalServerError, config.Response{Message: "internal server error"})
			return
		}

		c.JSON(http.StatusOK, config.Response{Message: "berhasil update pengaturan"})
	}

	return gin.HandlerFunc(handler)
}
