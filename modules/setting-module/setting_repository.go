package settingmodule

import (
	"fmt"
	"svc-order-ticket-go/config"
)

func GetAllSettings(app *config.App) ([]*Setting, error) {
	var setting []*Setting
	result := app.GormDB.Table("settings").Find(&setting)
	if result.Error != nil {
		fmt.Printf("[ERROR] error get setting - %s at modules/setting-modeules/setting_repository.go - GetAllSettings()", result.Error)
		return nil, result.Error
	}
	return setting, nil
}

func GetSettingByName(app *config.App, name string) (*Setting, error) {
	var setting Setting
	result := app.GormDB.Where("attribute = ?", name).Find(&setting)
	if result.Error != nil {
		fmt.Printf("[ERROR] Get kuota ticket - %v at modules/setting-module/setting_repository.go - GetSettingByName()\n", result.Error)
		return nil, result.Error
	}
	if result.RowsAffected == 0 {
		fmt.Printf("[ERROR] Get kuota ticket - 'attribute %v not found' at modules/setting-module/setting_repository.go - GetSettingByName()\n", name)
		return nil, fmt.Errorf("")
	}
	return &setting, nil
}

func UpdateSettings(app *config.App, setting *Setting) error {
	result := app.GormDB.Save(&setting)
	if result.Error != nil {
		fmt.Printf("[ERROR] Update setting - %v at modules/setting-module/setting_repository.go - UpdateSettings()\n", result.Error)
		return result.Error
	}
	return nil
}
