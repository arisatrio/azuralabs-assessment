package settingmodule

const (
	AttributeKuota             = "kuota"
	AttributePrice             = "price"
	AttributeDiscount          = "discount"
	AttributeFlagDiscount      = "flag_discount"
	AttributeFlagMultipleOrder = "flag_multiple_order"
	AttributeDurationLimit     = "duration_limit"
)

type Setting struct {
	ID        int
	Type      string
	Attribute string `json:"attribute" form:"attribute"`
	Value     int    `json:"value" form:"value"`
}

func (setting *Setting) DisableMultipleOrder() bool {
	if setting.Value == 0 {
		return true
	} else {
		return false
	}
}
