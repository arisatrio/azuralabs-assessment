# Golang Application

Aplikasi pemesanan tiket

## How to Run
Sebelum menjalankan service ini, ada beberapa hal yang perlu diperhatikan:


1. Sesuaikan konfigurasi environment variabel pada file run.sh.
    - DBUSER=
    - DBPASS=
    - DBHOST=
    - DBPORT=
    - DBNAME=

    Sesuaikan variabel-variabel diatas dengan komputer anda.

2. Untuk menjalan aplikasi, pastikan anda berada pada directory ini kemudian masukkan perintah: sh run.sh

## Documentation
- File collection_api_azuralabs_assessment.json Koleksi API yang di export dari postman yang dapat digunakan untuk testing  endpoint. File tersebut lengkap berisikan example response untuk setiap endpoint.
- File azuralabs_assessment.sql merupakan file database yang digunakan untuk menjalankan service ini.
- Dokumentasi selengkapnya: https://drive.google.com/drive/folders/1Xbbd8e4vmaF8GAMnfM8-741uW0MyU1q9?usp=sharing


Thank you



