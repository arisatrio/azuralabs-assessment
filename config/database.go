package config

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	"github.com/go-sql-driver/mysql"
	gormsql "gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func SqlConnection() *sql.DB {
	dbAddress := fmt.Sprintf("%s:%s", os.Getenv("DBHOST"), os.Getenv("DBPORT"))
	cfg := mysql.Config{
		User:   os.Getenv("DBUSER"),
		Passwd: os.Getenv("DBPASS"),
		Net:    "tcp",
		Addr:   dbAddress,
		DBName: os.Getenv("DBNAME"),
	}

	sqlConn, err := sql.Open("mysql", cfg.FormatDSN())
	if err != nil {
		log.Fatal(err)
	}

	pingErr := sqlConn.Ping()
	if pingErr != nil {
		log.Fatal(pingErr)
	}

	return sqlConn
}

func GormConnection(sqlDB *sql.DB) *gorm.DB {
	gormDB, err := gorm.Open(gormsql.New(gormsql.Config{
		Conn: sqlDB,
	}), &gorm.Config{})

	if err != nil {
		log.Fatal(err)
	}

	return gormDB
}
