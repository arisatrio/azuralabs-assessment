package config

import (
	"database/sql"

	"gorm.io/gorm"
)

const (
	HttpCodeError      = 500
	HttpCodeBadRequest = 400
	HttpCodeStatusOK   = 200
)

type App struct {
	SqlDB  *sql.DB
	GormDB *gorm.DB
}

type Response struct {
	Message string      `json:"message"`
	Data    interface{} `json:"data,omitempty"`
}

type ResponseError struct {
	Code    int
	Message string
}
